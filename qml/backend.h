#ifndef __FMRXQT_BACKEND_H
#define __FMRXQT_BACKEND_H

/*
 *  fmrxqt - QML UI for Javier S. Pedro's "fmrx" utility
 *  Copyright (C) 2011 Thomas Perl <thp.io/about>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include <QtCore>

class FMRX : public QObject
{
    Q_OBJECT

private:
    QProcess *_process;
    qreal _frequency;

public:
    FMRX();

    ~FMRX() { stop(); }

    qreal frequency() {
        return _frequency;
    }

    void setFrequency(qreal frequency) {
        if (frequency != _frequency) {
            _frequency = frequency;
            emit frequencyChanged();
        }
    }

    Q_PROPERTY(qreal frequency READ frequency WRITE setFrequency NOTIFY frequencyChanged)

public slots:
    void start();
    void stop();
    void sendFrequency();

signals:
    void frequencyChanged();
};

#endif
