
/*
 *  fmrxqt - QML UI for Javier S. Pedro's "fmrx" utility
 *  Copyright (C) 2011 Thomas Perl <thp.io/about>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "backend.h"

FMRX::FMRX()
    : _process(NULL),
      _frequency(0.)
{
    QObject::connect(this, SIGNAL(frequencyChanged()),
                     this, SLOT(sendFrequency()));
}

void
FMRX::start()
{
    if (_process == NULL) {
        _process = new QProcess(this);
        _process->setProcessChannelMode(QProcess::MergedChannels);
        //_process->start("/home/thp/src/fmrx/qml/fmrx-simple.py --slave");
        _process->start("/usr/bin/fmrx");
        sendFrequency();
    }
}

void
FMRX::stop()
{
    if (_process != NULL) {
        if (_process->state() == QProcess::Running) {
            _process->write("quit\n");
        }
        _process->waitForFinished();
        _process->deleteLater();
        _process = NULL;
    }
}

void
FMRX::sendFrequency()
{
    if (_process != NULL) {
        qDebug() << "would send frequency: " << _frequency;
        QString s = QString("%1\n").arg(_frequency);

        _process->write(s.toAscii());
    }
}

