
/*
 *  fmrxqt - QML UI for Javier S. Pedro's "fmrx" utility
 *  Copyright (C) 2011 Thomas Perl <thp.io/about>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

import QtQuick 1.0

Image {
    id: root
    source: 'background.png'
    property bool enabled: false

    property int ten: 8
    property int one: 0
    property int fract: 0

    property real frequency: ten * 10 + one + fract * .1

    Binding {
        target: fmrx
        property: 'frequency'
        value: root.frequency
    }

    onFrequencyChanged: {
        console.log('new frequency: ' + frequency);
    }

    Row {
        id: row

        x: 150
        y: 170

        Repeater {
            model: 3

            Spinner {
                property int outerIndex: index

                height: 200
                width: (outerIndex == 0) ? 120 : 80

                model: {
                    if (outerIndex == 0) {
                        [8, 9, 10]
                    } else {
                        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                    }
                }

                onCurrentIndexChanged: {
                    var value = model[currentIndex];

                    if (outerIndex == 0) {
                        root.ten = value;
                    } else if (outerIndex == 1) {
                        root.one = value;
                    } else if (outerIndex == 2) {
                        root.fract = value;
                    }
                }

                itemHeight: 60

                delegate: Text {
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    horizontalAlignment: (outerIndex==0)?Text.AlignRight:Text.AlignHCenter
                    text: modelData
                    font.pixelSize: PathView.isCurrentItem ? 100 : 40
                    color: 'white'
                    Behavior on font.pixelSize { NumberAnimation { } }
                }
            }
        }
    }

    Image {
        source: 'spinner-fg.png'
        anchors.centerIn: row
        opacity: .7
    }

    MouseArea {
        x: 200
        y: 500
        width: 244
        height: 130
        onClicked: {
            root.enabled = !root.enabled
            if (root.enabled) {
                fmrx.start()
            } else {
                fmrx.stop()
            }
        }

        Image {
            source: root.enabled?'on.png':'off.png'
            anchors.fill: parent
        }
    }
}

