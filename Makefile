CFLAGS?=-Os -Wall -ggdb
LDFLAGS?=-Wl,--as-needed

FMRX_PKGS:=glib-2.0 alsa libpulse-mainloop-glib libresource0-glib
FMRX_CFLAGS:=$(shell pkg-config --cflags $(FMRX_PKGS))
FMRX_LIBS:=$(shell pkg-config --libs $(FMRX_PKGS))

all: fmrx qmlui

qmlui:
	make -C qml

fmrx: fmrx.o
	$(CC) $(FMRX_LDFLAGS) $(LDFLAGS) -o $@ $+ $(LIBS) $(FMRX_LIBS)

%.o: %.c
	$(CC) $(FMRX_CFLAGS) $(CFLAGS) -o $@ -c $<

clean:
	rm -f *.o fmrx
	-make -C qml distclean

.PHONY: all qmlui clean
.DEFAULT: all

