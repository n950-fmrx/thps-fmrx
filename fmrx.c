/*
 *  fmrx - a small application to tune and listen to FM Radio on a N950
 *  Copyright (C) 2011 Javier S. Pedro <maemo@javispedro.com>
 *  Copyright (C) 2011 Thomas Perl <thp.io/about>
 *
 *  libresource stuff based on Nokia's libresource fmradio example
 *  Copyright (C) 2010 Nokia Corporation
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <signal.h>
#include <math.h>

#include <sys/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>

#include <glib.h>

#include <alsa/asoundlib.h>
#include <linux/videodev2.h>
#include <dbus/dbus-glib.h>
#include <pulse/glib-mainloop.h>
#include <pulse/pulseaudio.h>
#include <resource.h>

#define GETTEXT_PACKAGE "fmrx"
#define PA_STREAM_NAME  "FM Radio"

#define ALSA_MIXER_NAME "hw:2"
#define ALSA_PCM_CAPTURE_NAME "hw:2,0"

#define TUNER_DEVICE "/dev/radio0"

#define TRANSFER_BLOCK_MIN_SIZE (4096*4)

static GMainLoop *main_loop;
static GIOChannel *g_stdin;
static gdouble frequency = 0.0f;
static gboolean verbose = FALSE;
static gboolean read_rds = FALSE;

static pa_glib_mainloop *pa_loop;
static pa_context *pa_ctx;

static bool res_granted = false;
static bool radio_on = false;

static snd_pcm_t *a_pcm = NULL;
static guint a_fd_watch = 0;

static int tuner_fd = -1;
static bool tuner_precise;
static unsigned long tuner_min, tuner_max;

static guint rds_watch = 0;
static guint rds_timer = 0;
static unsigned short rds_pi;
static bool rds_tp;
static unsigned char rds_pty;
static char rds_psn[9] = { '\0' };

static pa_stream *pa_s = NULL;

static GOptionEntry options[] = {
	{ "frequency", 'f', 0, G_OPTION_ARG_DOUBLE, &frequency, "Set the frequency to <freq> MHz", "freq" },
	{ "rds", 'r', 0, G_OPTION_ARG_NONE, &read_rds, "Print RDS data", NULL },
	{ "verbose", 'v', 0, G_OPTION_ARG_NONE, &verbose, "Be verbose", NULL },
	{ NULL }
};


static inline unsigned long v4l_to_hz(unsigned f)
{
	if (tuner_precise) {
		/* f * 62.5 */
		return (f * 125UL) / 2;
	} else {
		return f * 62500UL;
	}
}

static inline unsigned hz_to_v4l(unsigned long f)
{
	if (tuner_precise) {
		/* f / 62.5 */
		return (f * 2UL) / 125;
	} else {
		return f / 62500;
	}
}

static inline unsigned long mhz_to_hz(double f)
{
	return f * 1000000.0;
}

/** Sets mixer switch <name> to <value> */
static bool mixer_set_enum_value(snd_hctl_t *mixer,
	const char * name, const char * value)
{
	int err;

	snd_ctl_elem_id_t *id;
	snd_ctl_elem_id_alloca(&id);
	snd_ctl_elem_id_set_interface(id, SND_CTL_ELEM_IFACE_MIXER);
	snd_ctl_elem_id_set_name(id, name);

	snd_hctl_elem_t *elem = snd_hctl_find_elem(mixer, id);
	if (!elem) {
		g_critical("Couldn't find mixer element '%s'", name);
		return false;
	}

	snd_ctl_elem_info_t *info;
	snd_ctl_elem_info_alloca(&info);
	snd_ctl_elem_info_set_id(info, id);
	err = snd_hctl_elem_info(elem, info);
	if (err) {
		g_critical("Couldn't get mixer element '%s' information (%d)", name,
			err);
		return false;
	}

	long value_idx = -1;
	int i, items = snd_ctl_elem_info_get_items(info);
	for (i = 0; i < items; i++) {
		snd_ctl_elem_info_set_item(info, i);
		err = snd_hctl_elem_info(elem, info);
		if (err) {
			g_critical("Couldn't get mixer element '%s' information (%d)", name,
				err);
			return false;
		}
		if (strcmp(snd_ctl_elem_info_get_item_name(info), value) == 0) {
			value_idx = i;
			break;
		}
	}
	if (value_idx < 0) {
		g_critical("Couldn't find mixer '%s' value '%s'", name, value);
		return false;
	}

	snd_ctl_elem_value_t *control;
	snd_ctl_elem_value_alloca(&control);
	snd_ctl_elem_value_set_id(control, id);

	items = snd_ctl_elem_info_get_count(info);
	for (i = 0; i < items; i++) {
		snd_ctl_elem_value_set_enumerated(control, i, value_idx);
	}

	err = snd_hctl_elem_write(elem, control);
	if (err) {
		g_critical("Couldn't set mixer element '%s' (%d)", name, err);
		return false;
	}

	return true;
}

static bool configure_mixer(bool on)
{
	snd_hctl_t *mixer;
	int err;
	bool res;

	err = snd_hctl_open(&mixer, ALSA_MIXER_NAME, 0);
	if (err < 0) {
		g_critical("Failed to open ALSA mixer '%s' (%d)", ALSA_MIXER_NAME, err);
		return false;
	}
	err = snd_hctl_load(mixer);
	if (err < 0) {
		g_critical("Failed to load ALSA hmixer elements (%d)", err);
		snd_hctl_close(mixer);
		return false;
	}

	if (on) {
		res = mixer_set_enum_value(mixer, "Mode Switch", "Rx");
		res &= mixer_set_enum_value(mixer, "Codec Mode", "FmRx");
		res &= mixer_set_enum_value(mixer, "Audio Switch", "Digital");
	} else {
		res = mixer_set_enum_value(mixer, "Codec Mode", "Bt");
		res &= mixer_set_enum_value(mixer, "Mode Switch", "Off");
	}

	/* TODO: "Region Switch" for Japan freqs. */

	snd_hctl_close(mixer);

	return res;
}

static bool configure_tuner(bool on)
{
	if (on) {
		struct v4l2_tuner tuner = { 0 };
		int res;

		if (tuner_fd == -1) {
			tuner_fd = open(TUNER_DEVICE, O_RDONLY);
		}

		if (tuner_fd == -1) {
			g_critical("Couldn't open V4L2 tuner");
			return false;
		}

		tuner.index = 0;
		res = ioctl(tuner_fd, VIDIOC_G_TUNER, &tuner);
		if (res < 0) {
			g_critical("Couldn't get V4L2 tuner information");
			return false;
		}

		if (tuner.type != V4L2_TUNER_RADIO) {
			g_critical("Not a radio tuner\n");
			return false;
		}

		tuner_precise = (tuner.capability & V4L2_TUNER_CAP_LOW) ?
			                    TRUE : FALSE;
		tuner_min = v4l_to_hz(tuner.rangelow);
		tuner_max = v4l_to_hz(tuner.rangelow);

		if (frequency != 0.0) {
			struct v4l2_frequency t_freq = { 0 };
			t_freq.tuner = 0;
			t_freq.type = V4L2_TUNER_RADIO;
			t_freq.frequency = hz_to_v4l(mhz_to_hz(frequency));
			res = ioctl(tuner_fd, VIDIOC_S_FREQUENCY, &t_freq);
			if (res < 0) {
				g_warning("Failed to tune");
				/* Not fatal... */
			}
			g_message("Tuned to %.1f Mhz", frequency);
		}
		return true;
	} else {
		if (tuner_fd != -1) {
			close(tuner_fd);
			tuner_fd = -1;
		}
		return true;
	}
}

static gboolean capture_callback(GIOChannel *source, GIOCondition condition,
	gpointer data)
{
	snd_pcm_sframes_t avail, got;
	snd_pcm_uframes_t toread;
	void *buffer;
	size_t bytes, gotbytes;
	int err;

	if (pa_stream_get_state(pa_s) != PA_STREAM_READY) {
		/* Not yet ready, wait until next callback. */
		return TRUE;
	}

	avail = snd_pcm_avail_update(a_pcm);
	if (avail < 0) {
		g_critical("ALSA stream avail failed (%d)", (int) avail);
		err = snd_pcm_recover(a_pcm, avail, 1);
		if (err < 0) {
			g_critical("ALSA stream failed to recover (%d)", err);
			g_main_loop_quit(main_loop);
			return FALSE;
		}
		return TRUE; /* Retry next time. */
	}

	bytes = snd_pcm_frames_to_bytes(a_pcm, avail);
	err = pa_stream_begin_write(pa_s, &buffer, &bytes);
	if (err < 0) {
		g_critical("PA failed to begin write (%d %s)", err, pa_strerror(err));
		g_main_loop_quit(main_loop);
		return FALSE;
	}

	toread = snd_pcm_bytes_to_frames(a_pcm, bytes);

	got = snd_pcm_readi(a_pcm, buffer, toread);
	if (got < 0) {
		g_critical("ALSA failed to read (%d)", (int) got);
		err = snd_pcm_recover(a_pcm, got, 1);
		if (err < 0) {
			g_critical("ALSA stream failed to recover (%d)", err);
			g_main_loop_quit(main_loop);
			return FALSE;
		}
		return TRUE; /* Retry next time. */
	}

	gotbytes = snd_pcm_frames_to_bytes(a_pcm, got);
	err = pa_stream_write(pa_s, buffer, gotbytes, NULL, 0, PA_SEEK_RELATIVE);
	if (err < 0) {
		g_critical("PA failed to write (%d)", err);
		g_main_loop_quit(main_loop);
		return FALSE;
	}

	return TRUE;
}

/** Equivalent to "arecord -Dhw:2,0 -f S16_LE -r 48000 -c 2" */
static bool configure_capture(bool on)
{
	int err;

	if (on) {
		err = snd_pcm_open(&a_pcm, ALSA_PCM_CAPTURE_NAME,
			SND_PCM_STREAM_CAPTURE,	SND_PCM_NONBLOCK);
		if (err < 0) {
			g_critical("Failed to open ALSA PCM device '%s' (%d)", ALSA_PCM_CAPTURE_NAME, err);
			return false;
		}

		/* -f S16_LE -r 48000 -c 2; latency up to 0.3 seconds */
		err = snd_pcm_set_params(a_pcm, SND_PCM_FORMAT_S16_LE,
			SND_PCM_ACCESS_RW_INTERLEAVED, 2, 48000, 0, 300000);
		if (err < 0) {
			g_critical("Failed to open ALSA PCM device '%s' (%d)", ALSA_PCM_CAPTURE_NAME, err);
			snd_pcm_close(a_pcm);
			return false;
		}

		struct pollfd polls[1];
		int count = snd_pcm_poll_descriptors(a_pcm, polls, 1);
		if (count != 1) {
			g_critical("Not able to get poll FD from ALSA");
			snd_pcm_close(a_pcm);
			return false;
		}

		GIOChannel *channel = g_io_channel_unix_new(polls[0].fd);
		a_fd_watch = g_io_add_watch(channel, polls[0].events,
			capture_callback, NULL);
		g_io_channel_unref(channel);

		err = snd_pcm_start(a_pcm);
		if (err < 0) {
			g_critical("Couldn't start PCM device (%d)", err);
			snd_pcm_close(a_pcm);
			return false;
		}

		return true;
	} else {
		if (a_fd_watch) {
			g_source_remove(a_fd_watch);
			a_fd_watch = 0;
		}
		if (a_pcm) {
			snd_pcm_close(a_pcm);
			a_pcm = NULL;
		}

		return true;
	}
}

/** Equivalent to "pacat -p" */
static bool configure_playback(bool on)
{
	int res;

	if (on) {
		pa_sample_spec spec = {
			.format = PA_SAMPLE_S16LE,
			.rate = 48000,
			.channels = 2
		};

		pa_s = pa_stream_new(pa_ctx, PA_STREAM_NAME, &spec, NULL);
		if (!pa_s) {
			g_warning("Failed to create output stream");
			return false;
		}

		res = pa_stream_connect_playback(pa_s, NULL, NULL, 0, NULL, NULL);
		if (res != 0) {
			g_warning("Failed to connect output stream: %s", pa_strerror(res));
			return false;
		}
	} else {
		if (pa_s) {
			pa_stream_disconnect(pa_s);
			pa_stream_unref(pa_s);
			pa_s = NULL;
		}
	}

	return true;
}

static gboolean rds_callback(GIOChannel *source, GIOCondition condition,
	gpointer data)
{
	struct v4l2_rds_data rds;
	int res = read(tuner_fd, &rds, sizeof(rds));
	if (res < sizeof(rds)) {
		g_warning("Failed to read RDS information");
		return TRUE; /* Not fatal */
	}

#if 0
	/* Driver always send this? */
	if (rds.block & V4L2_RDS_BLOCK_ERROR) {
		g_warning("Incorrectable error in RDS block");
		return TRUE; /* Not fatal. */
	}
#endif

	static char group, spare;
	int block = rds.block & V4L2_RDS_BLOCK_MSK;

	switch (block) {
		case V4L2_RDS_BLOCK_A:
			rds_pi = (rds.msb << 8) | rds.lsb;
		break;
		case V4L2_RDS_BLOCK_B:
			group = (rds.msb >> 3) & 0x1f;
			spare = rds.lsb & 0x1f;

			rds_tp = (rds.msb >> 2) & 1;
			rds_pty = ((rds.msb << 3) & 0x18) | ((rds.lsb >> 5) & 0x7);
		break;
		case V4L2_RDS_BLOCK_C:

		break;
		case V4L2_RDS_BLOCK_D:
			g_message("Oh, a D block!");
			/* Fallthrough */
		case V4L2_RDS_BLOCK_C_ALT: /* I fail to understand why this is not D */
			switch (group) {
				case 0: /* Type 0A */
					rds_psn[2*(spare & 0x03)+0] = rds.msb;
					rds_psn[2*(spare & 0x03)+1] = rds.lsb;
				break;
			}
		break;
		default:
			printf("?\n");
		break;
	}

	return TRUE;
}

static gboolean rds_timer_callback(gpointer data)
{
	g_print("RDS: PI=%hu PSN='%s'\n", rds_pi, rds_psn);
	return TRUE;
}

static bool configure_rds(bool on)
{
	if (on) {
		GIOChannel *channel = g_io_channel_unix_new(tuner_fd);
		rds_watch = g_io_add_watch(channel, G_IO_IN, rds_callback, NULL);
		g_io_channel_unref(channel);
		if (read_rds) {
			rds_timer = g_timeout_add_seconds(5, rds_timer_callback, NULL);
		}
	} else {
		if (rds_watch) {
			g_source_remove(rds_watch);
			rds_watch = 0;
		}
		if (rds_timer) {
			g_source_remove(rds_timer);
			rds_timer = 0;
		}
	}

	return true;
}

static void enable_radio()
{
	g_debug("Enabling radio...");

	radio_on = true;

	if (!configure_mixer(true)) return;
	if (!configure_tuner(true)) return;
	if (!configure_capture(true)) return;
	if (!configure_playback(true)) return;
	if (!configure_rds(true)) return;

	g_message("Radio enabled");
}

static void disable_radio()
{
	bool res = true;
	g_debug("Disabling radio...");

	radio_on = false;

	res &= configure_rds(false);
	res &= configure_playback(false);
	res &= configure_capture(false);
	res &= configure_tuner(false);
	res &= configure_mixer(false);

	if (res) g_message("Radio disabled");
}

static void update_radio_state()
{
	bool pa_ready = pa_context_get_state(pa_ctx) == PA_CONTEXT_READY;
	bool should_enable = pa_ready && res_granted;

	if (should_enable && !radio_on) {
		enable_radio();
	} else if (!should_enable && radio_on) {
		disable_radio();
	}
}

static void grant_callback(resource_set_t *resource_set, uint32_t resources,
	void *userdata)
{
	res_granted = resources & RESOURCE_AUDIO_PLAYBACK;
	g_debug("Resources have been %s", res_granted ? "granted" : "lost");
	update_radio_state();
}

static void error_callback(resource_set_t *resource_set, uint32_t errcod,
	const char *errmsg, void *userdata)
{
	g_critical("Failed to acquire resources: %s", errmsg);
	g_main_loop_quit(main_loop);
}

static void pa_state_callback(pa_context *c, void *userdata)
{
	switch (pa_context_get_state(c)) {
	case PA_CONTEXT_READY:
		g_debug("PA context ready");
		update_radio_state();
	break;
	case PA_CONTEXT_FAILED:
		g_critical("PA context failed");
		g_main_loop_quit(main_loop);
	break;
	default:
	break;
	}
}

static void signal_handler(int signal)
{
	/* Quit gracefully. */
	g_main_loop_quit(main_loop);
}

static void log_handler(const gchar *log_domain, GLogLevelFlags log_level, const gchar *message, gpointer user_data)
{
	g_print("%s\n", message);
}

static gboolean stdin_read_handler(GIOChannel *source,
	GIOCondition condition, gpointer data)
{
	int bytes_read;
        GString *input_line = g_string_new("");
        gsize terminator_pos;
        gdouble new_value;
        gchar *endptr;

        do {
            if (g_io_channel_read_line_string(source, input_line,
                    &terminator_pos, NULL) != G_IO_STATUS_NORMAL) {
                g_warning("Cannot read string from stdin");
                break;
            }
            input_line->str[terminator_pos] = '\0';

            if (strcmp(input_line->str, "quit") == 0) {
                g_print("exiting after request from stdin\n");
                g_main_loop_quit(main_loop);
            }

            //g_debug("input line: '%s'\n", input_line->str);
            new_value = g_ascii_strtod(input_line->str, &endptr);
            //g_print("double value read from stdin: %f\n", new_value);

            if (errno == ERANGE || endptr == input_line->str) {
                g_warning("Ignoring invalid value from stdin.");
                break;
            }

            /* Tune in! */
            frequency = new_value;
            configure_tuner(true);
        } while (0);

        g_string_free(input_line, TRUE);
	return TRUE;
}

int main(int argc, char **argv)
{
	GError *error = NULL;
	GOptionContext *context;
	resource_set_t *resource_set;
	int res;

	main_loop = g_main_loop_new(NULL, FALSE);

	/* FIXME: Only do that in "slave mode" */
	g_stdin = g_io_channel_unix_new(0);
	g_io_add_watch(g_stdin, G_IO_IN, stdin_read_handler, NULL);

	context = g_option_context_new(" - listen to FM Radio");
	g_option_context_add_main_entries(context, options, GETTEXT_PACKAGE);

	if (!g_option_context_parse(context, &argc, &argv, &error)) {
		g_printerr("Option parsing failed: %s", error->message);
		return 1;
	}

	if (verbose) {
		g_log_set_default_handler(log_handler, NULL);
	}

	/* Trap quit signals to die gracefully. */
	signal(SIGTERM, signal_handler);
	signal(SIGINT, signal_handler);
	signal(SIGHUP, signal_handler);

	/* Set up the connection to PA. */
	pa_loop = pa_glib_mainloop_new(NULL);
	pa_ctx = pa_context_new(pa_glib_mainloop_get_api(pa_loop), "fmrx");
	pa_context_set_state_callback(pa_ctx, pa_state_callback, NULL);
	res = pa_context_connect(pa_ctx, NULL, 0, NULL);
	if (res != 0) {
		g_critical("Couldn't connect to PA");
		return 1;
	}

	/* Ok, grab the resources. */
	resource_set = resource_set_create("player", RESOURCE_AUDIO_PLAYBACK,
		0, 0, grant_callback, NULL);
	resource_set_configure_error_callback(resource_set, error_callback, NULL);
#if 0
	resource_set_configure_audio(resource_set, "fmradio", /*getpid(),
		PA_STREAM_NAME);*/
#else
	resource_set_configure_audio(resource_set, "fmradio", 0, NULL);
#endif

	resource_set_acquire(resource_set);

	g_main_loop_run(main_loop);

	if (radio_on) disable_radio();
	resource_set_release(resource_set);
	resource_set_destroy(resource_set);
	g_main_loop_unref(main_loop);

	return 0;
}

